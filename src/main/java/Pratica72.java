import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import utfpr.ct.dainf.if62c.pratica.ContadorPalavras;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * IF62C - Fundamentos de Programação 2
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica72 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Insira aqui o filepath: ");
        String arqtxt = scanner.next();
        try{
            FileReader arq = new FileReader(arqtxt + ".txt"); 
            ContadorPalavras cp = new ContadorPalavras(arq);
            PrintWriter arqOut = new PrintWriter(new FileWriter(arqtxt + "out.txt"));
            
                Set<Map.Entry<String,Integer>> set = cp.getPalavras().entrySet();
                
                for(Map.Entry<String,Integer> entry: set){
                    System.out.println(entry.getKey() + " : " + entry.getValue());
                    arqOut.println(entry.getKey() + " : " + entry.getValue());
                    }
            arq.close();
            arqOut.close();
        }
        catch(IOException e){
            System.out.printf("Erro na abertura do arquivo %s\n", e.getMessage());
        }
        
    }
 }    
    /*String linha = "Amei ele, ele me amou, amei ele";
    String linha1 = "Tá chegando caralho!";
    String[] termo;
    ArrayList<String> list = new ArrayList<>();
    Integer cont = 1;
    termo = linha.split(" ");
    
    list.addAll(Arrays.asList(termo));
    
    termo = linha1.split(" ");
    
    list.addAll(Arrays.asList(termo));
    
    for(String s: list){
        System.out.println(s);
        }
    
      System.out.println(++cont);
    }*/
    
    
    
    


