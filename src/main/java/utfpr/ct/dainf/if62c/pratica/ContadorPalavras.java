/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author samu
 */
public class ContadorPalavras {
    private BufferedReader reader;
    
    
    
    /*public ContadorPalavras(String arq_txt) throws FileNotFoundException{
         this.arq_txt = arq_txt;
         reader = new BufferedReader(new FileReader(arq_txt));
    }*/
    public ContadorPalavras(FileReader leitor){
        reader = new BufferedReader(leitor);
    }
    
    public HashMap getPalavras() throws IOException{
        HashMap<String,Integer> mapa = new HashMap<>();
        ArrayList<String> list = new ArrayList<>();
        String linha;
        String[] words;
        
        while((linha = reader.readLine()) != null){
            words = linha.split(" ");
            list.addAll(Arrays.asList(words));
        }
        int cont;
        for(String p:list){
            cont = 0;
            for(String s: list){
                if(s.equalsIgnoreCase(p)){
                    mapa.put(p,++cont);
                }
            }
        }
        return mapa;
    }
    
}
